FROM openjdk:8-alpine
RUN mkdir /opt/investimentos

WORKDIR /opt/investimentos
COPY target/Api-Investimentos.jar .

CMD ["java", "-jar", "Api-Investimentos.jar"]