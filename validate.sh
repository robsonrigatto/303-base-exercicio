#!/bin/bash

result=$(curl --user validate:validate -X POST -F "jenkinsfile=<Jenkinsfile" http://jenkins-itau.mastertech.com.br/pipeline-model-converter/validate) 
echo "Resultado: $result"

status=0
if [[ $result == *"Errors encountered"* ]]; then
    status=1
fi

exit $status